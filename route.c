#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "path.h"
#include "route.h"
#include "dir.h"
#include "json.h"
#include "parser.h"
#include "strdup.h"

extern char *dir;

route_marker *route_marker_create()
{
	route_marker *r = malloc(sizeof(route_marker));
	r->colors = malloc(32 * 16 * sizeof(char));
	r->data = malloc(32 * 16384 * sizeof(char));
	r->xs = malloc(32 * sizeof(double));
	r->ys = malloc(32 * sizeof(double));
	r->sizes = malloc(32 * sizeof(double));
	r->series = malloc(32 * 16 * sizeof(char));
	r->text = malloc(32 * 256 * sizeof(char));
	r->textcolors = malloc(32 * 16 * sizeof(char));
	return r;
}

void route_read(char *name, route_marker *r)
{
	char fn[80];
	sprintf(fn, "%s/routes/%s.rte", dir, name);

	FILE *f = fopen(fn, "r");

	if (f == NULL)
	{
		fprintf(stderr, "rsm: unrecognized route marker '%s'\n", name);
		route_marker_free(r);
		exit(1);
	}

	json_value *val = read_json_file(f);

	json_value *paths = json_value_find_key(val, "paths");
	json_value *texts = json_value_find_key(val, "text");
	if (paths->type != json_array || texts->type != json_array)
	{
		fprintf(stderr, "rsm: invalid route marker format '%s'\n", name);
		route_marker_free(r);
		exit(1);
	}

	r->len = paths->u.array.length;
	r->texts = texts->u.array.length;

	int i;
	for (i = 0; i < r->len; ++i)
	{
		json_value *c = paths->u.array.values[i];
		r->colors[i] = parse_string(json_value_find_key(c, "color"));
		r->data[i] = parse_string(json_value_find_key(c, "path"));
	}
	for (i = 0; i < r->texts; ++i)
	{
		json_value *c = texts->u.array.values[i];
		json_value *pos = json_value_find_key(c, "position");
		r->xs[i] = parse_number(pos->u.array.values[0]);
		r->ys[i] = parse_number(pos->u.array.values[1]);
		r->textcolors[i] = parse_string(json_value_find_key(c, "color"));
		r->sizes[i] = parse_number(json_value_find_key(c, "size"));
		r->series[i] = parse_string(json_value_find_key(c, "series"));
		r->text[i] = parse_string(json_value_find_key(c, "text"));
	}

	json_value_free(val);
	fclose(f);
}

void route_marker_free(route_marker *rm)
{
	free(rm->colors);
	free(rm->data);
	free(rm->xs);
	free(rm->ys);
	free(rm->sizes);
	free(rm->text);
	free(rm->textcolors);
	free(rm->series);
	free(rm);
}
