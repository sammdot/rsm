#include <math.h>
#include <stdio.h>
#include <cairo.h>
#include <cairo-ft.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include "arrow.h"
#include "icon.h"
#include "librsm.h"
#include "path.h"
#include "render.h"
#include "route.h"

static const double PI = 3.1415926535897932384626433832795;

static double max(double a, double b)
{
	if (a > b) return a;
	return b;
}

void element_dimension(element *e)
{
	if (e == NULL) return;

	cairo_surface_t *s = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 0, 0);
	cairo_t *cr = cairo_create(s);

	switch (e->type_)
	{
		case TYPE_XSTACK:
		case TYPE_YSTACK:
		case TYPE_ZSTACK:
		{
			stack *s = (stack *)e;
			int i;
			element *c;
			double Wi = 0, Hi = 0, Ws[4] = {0}, Hs[4] = {0};
			for (i = 0; i < vector_size(s->children); ++i)
			{
				c = (element *)vector_get(s->children, i);
				element_dimension(c);

				if (c == NULL) continue;

				if (e->type_ == TYPE_XSTACK || e->type_ == TYPE_ZSTACK)
				{
					Hs[c->valign] = max(Hs[c->valign], c->geom->height + c->margin[EDGE_TOP] + c->margin[EDGE_BOTTOM]);
					Hi = max(s->height, max(Hs[0], max(Hs[1], max(Hs[2], Hs[3]))));
				}
				else Hi = max(s->height, Hi + c->margin[EDGE_TOP] + c->margin[EDGE_BOTTOM] + c->geom->height);

				if (e->type_ == TYPE_YSTACK || e->type_ == TYPE_ZSTACK)
				{
					Ws[c->halign] = max(Ws[c->halign], c->geom->width + c->margin[EDGE_LEFT] + c->margin[EDGE_RIGHT]);
					Wi = max(s->width, max(Ws[0], max(Ws[1], max(Ws[2], Ws[3]))));
				}
				else Wi = max(s->width, Wi + c->margin[EDGE_LEFT] + c->margin[EDGE_RIGHT] + c->geom->width);
			}
			s->inwidth = Wi;
			s->inheight = Hi;

			for (i = 0; i < vector_size(s->children); ++i)
			{
				c = (element *)vector_get(s->children, i);
				if (c->type_ != TYPE_XSTACK &&
				    c->type_ != TYPE_YSTACK &&
				    c->type_ != TYPE_ZSTACK) continue;

				stack *st = (stack *)c;

				if ((e->type_ == TYPE_XSTACK || e->type_ == TYPE_ZSTACK) &&
				    c->valign == VALIGN_FULL)
				{
					double gap = (s->inheight - c->geom->height) / 2;
					st->padding[EDGE_TOP] += gap;
					st->padding[EDGE_BOTTOM] += gap;
					element_dimension(c);
				}
				if ((e->type_ == TYPE_YSTACK || e->type_ == TYPE_ZSTACK) &&
				    c->halign == HALIGN_FULL)
				{
					double gap = (s->inwidth - c->geom->width) / 2;
					st->padding[EDGE_LEFT] += gap;
					st->padding[EDGE_RIGHT] += gap;
					element_dimension(c);
				}
			}

			s->geom->width = Wi + s->padding[EDGE_LEFT] + s->padding[EDGE_RIGHT] + (s->border->width + s->border->gap) * s->border->edges[EDGE_LEFT] + (s->border->width + s->border->gap) * s->border->edges[EDGE_RIGHT];
			s->geom->height = Hi + s->padding[EDGE_TOP] + s->padding[EDGE_BOTTOM] + (s->border->width + s->border->gap) * s->border->edges[EDGE_TOP] + (s->border->width + s->border->gap) * s->border->edges[EDGE_BOTTOM];
			break;
		}
		case TYPE_TEXT:
		{
			text *t = (text *)e;
			FT_Face f = font_from_series(t->series);
			if (f == NULL) break;
			cairo_font_face_t *font = cairo_ft_font_face_create_for_ft_face(f, FT_LOAD_NO_HINTING);
			cairo_set_font_face(cr, font);
			cairo_set_font_size(cr, 60);
			cairo_text_extents_t ext, aext;
			cairo_text_extents(cr, t->data, &ext);
			cairo_text_extents(cr, "A", &aext);
			t->scale = t->size / aext.height;
			t->geom->height = t->size;
			t->geom->width = ext.width * t->scale;
			t->lsb = ext.x_bearing * t->scale;
			cairo_font_face_destroy(font);
			break;
		}
		case TYPE_ARROW:
		{
			arrow *a = (arrow *)e;
			arrow_shape *arr = arrow_shape_create();
			arrow_read(a->name, arr);
			if (arr == NULL) break;
			cairo_save(cr);
			cairo_rotate(cr, a->angle * PI / 180.0);
			path_draw(cr, arr->path);
			cairo_restore(cr);
			double x1, y1, x2, y2, w, h, scale;
			cairo_fill_extents(cr, &x1, &y1, &x2, &y2);
			w = x2 - x1;
			h = y2 - y1;
			if (a->width == 0 && a->height == 0) scale = 1.0;
			else if (a->width == 0) scale = a->height / h;
			else if (a->height == 0) scale = a->width / w;
			else scale = (a->width / w > a->height / h) ? a->height / h : a->width / w;
			a->scale = scale;
			a->geom->width = scale * w;
			a->geom->height = scale * h;
			arrow_shape_free(arr);
			break;
		}
		case TYPE_ICON:
		{
			icon *ic = (icon *)e;
			icon_shape *is = icon_shape_create();
			icon_read(ic->name, is);
			if (is == NULL) break;
			double x1, y1, x2, y2, w, h, scale;
			path_draw(cr, is->path);
			cairo_path_extents(cr, &x1, &y1, &x2, &y2);
			cairo_new_path(cr);
			w = x2 - x1;
			h = y2 - y1;
			if (ic->width == 0 && ic->height == 0) scale = 1.0;
			else if (ic->width == 0) scale = ic->height / h;
			else if (ic->height == 0) scale = ic->width / w;
			else scale = (ic->width / w > ic->height / h) ? ic->height / h : ic->width / w;
			ic->scale = scale;
			ic->geom->width = scale * w;
			ic->geom->height = scale * h;
			icon_shape_free(is);
			break;
		}
		case TYPE_ROUTE:
		{
			route *r = (route *)e;
			route_marker *rm = route_marker_create();
			route_read(r->marker, rm);
			if (rm == NULL) break;
			int i;
			double x1, y1, x2, y2, w, h, scale;
			for (i = 0; i < rm->len; ++i)
				path_draw(cr, rm->data[i]);
			cairo_path_extents(cr, &x1, &y1, &x2, &y2);
			cairo_new_path(cr);
			w = x2 - x1;
			h = y2 - y1;
			if (r->width == 0 && r->height == 0) scale = 1.0;
			else if (r->width == 0) scale = r->height / h;
			else if (r->height == 0) scale = r->width / w;
			else scale = (r->width / w > r->height / h) ? r->height / h : r->width / w;
			r->scale = scale;
			r->geom->width = scale * w;
			r->geom->height = scale * h;
			route_marker_free(rm);
			break;
		}
	}

	cairo_destroy(cr);
	cairo_surface_destroy(s);
}

void element_position(element *e, double left, double top)
{
	if (e == NULL) return;

	switch (e->type_)
	{
		case TYPE_XSTACK:
		case TYPE_YSTACK:
		case TYPE_ZSTACK:
		{
			stack *s = (stack *)e;
			double X, Y, x, y;
			s->geom->left = left + s->margin[EDGE_LEFT];
			s->geom->top = top + s->margin[EDGE_TOP];

			X = s->geom->left + (s->border->width + s->border->gap) * s->border->edges[EDGE_LEFT] + s->padding[EDGE_LEFT];
			Y = s->geom->top + (s->border->width + s->border->gap) * s->border->edges[EDGE_TOP] + s->padding[EDGE_TOP];

			int i;
			element *c;
			for (i = 0; i < vector_size(s->children); ++i)
			{
				c = vector_get(s->children, i);
				if (c == NULL) continue;

				double kx = s->inwidth - (c->geom->width + c->margin[EDGE_LEFT] + c->margin[EDGE_RIGHT]);
				double ky = s->inheight - (c->geom->height + c->margin[EDGE_TOP] + c->margin[EDGE_BOTTOM]);

				int hal = (c->halign == HALIGN_FULL) ? 1 : c->halign;
				int val = (c->valign == VALIGN_FULL) ? 1 : c->valign;

				if (s->type_ == TYPE_YSTACK || s->type_ == TYPE_ZSTACK)
					x = X + hal * 0.5 * kx;
				else x = X;

				if (s->type_ == TYPE_XSTACK || s->type_ == TYPE_ZSTACK)
					y = Y + val * 0.5 * ky;
				else y = Y;

				if (s->type_ == TYPE_XSTACK)
					X += c->geom->width + c->margin[EDGE_LEFT] + c->margin[EDGE_RIGHT];
				if (s->type_ == TYPE_YSTACK)
					Y += c->geom->height + c->margin[EDGE_TOP] + c->margin[EDGE_BOTTOM];

				element_position(c, x, y);
			}
			break;
		}
		case TYPE_TEXT:
		case TYPE_ARROW:
		case TYPE_ICON:
		case TYPE_ROUTE:
		{
			e->geom->left = left + e->margin[EDGE_LEFT];
			e->geom->top = top + e->margin[EDGE_TOP];
			break;
		}
	}
}

void element_paint(element *e, cairo_t *cr, int debug)
{
	if (e == NULL || cr == NULL) return;

	switch (e->type_)
	{
		case TYPE_XSTACK:
		case TYPE_YSTACK:
		case TYPE_ZSTACK:
		{
			stack *s = (stack *)e;

			double b = s->border->width / 2;

			double Ri[4] = {0}, Ro[4] = {0};
			int i;
			for (i = 0; i < 4; ++i)
			{
				Ri[i] = (s->corner->radius[i] <= 0) ? 0 : s->corner->radius[i] - b;
				Ro[i] = (s->corner->radius[i] <= 0) ? 0 : (s->corner->radius[i] + s->border->gap) * s->corner->rounding[i];
			}

			double L = s->geom->left, T = s->geom->top, W = s->geom->width, H = s->geom->height;
			color *bkc = color_from_color_name(s->backcolor);
			color *bdc = color_from_color_name(s->border->color);
			if (bkc != NULL)
			{
				color_set_source(bkc, cr);

				if (Ro[CORNER_TOP_LEFT])
				{
					cairo_move_to(cr, L, T + Ro[CORNER_TOP_LEFT]);
					cairo_arc(cr, L + Ro[CORNER_TOP_LEFT], T + Ro[CORNER_TOP_LEFT], Ro[CORNER_TOP_LEFT], PI, 1.5 * PI);
				}
				else cairo_move_to(cr, L, T);

				if (Ro[CORNER_TOP_RIGHT])
				{
					cairo_line_to(cr, L + W - Ro[CORNER_TOP_RIGHT], T);
					cairo_arc(cr, L + W - Ro[CORNER_TOP_RIGHT], T + Ro[CORNER_TOP_RIGHT], Ro[CORNER_TOP_RIGHT], 1.5 * PI, 2 * PI);
				}
				else cairo_line_to(cr, L + W, T);

				if (Ro[CORNER_BOTTOM_RIGHT])
				{
					cairo_line_to(cr, L + W, T + H - Ro[CORNER_BOTTOM_RIGHT]);
					cairo_arc(cr, L + W - Ro[CORNER_BOTTOM_RIGHT], T + H - Ro[CORNER_BOTTOM_RIGHT], Ro[CORNER_BOTTOM_RIGHT], 0, 0.5 * PI);
				}
				else cairo_line_to(cr, L + W, T + H);

				if (Ro[CORNER_BOTTOM_LEFT])
				{
					cairo_line_to(cr, L + Ro[CORNER_BOTTOM_LEFT], T + H);
					cairo_arc(cr, L + Ro[CORNER_BOTTOM_LEFT], T + H - Ro[CORNER_BOTTOM_LEFT], Ro[CORNER_BOTTOM_LEFT], 0.5 * PI, PI);
				}
				else cairo_line_to(cr, L, T + H);

				cairo_close_path(cr);
				cairo_fill(cr);
			}

			if (bdc != NULL && b != 0)
			{
				L += b + s->border->gap * s->border->edges[EDGE_LEFT];
				T += b + s->border->gap * s->border->edges[EDGE_TOP];
				W -= 2 * b + s->border->gap * (s->border->edges[EDGE_LEFT] + s->border->edges[EDGE_RIGHT]);
				H -= 2 * b + s->border->gap * (s->border->edges[EDGE_TOP] + s->border->edges[EDGE_BOTTOM]);

				color_set_source(bdc, cr);
				cairo_set_line_width(cr, 2 * b);
				cairo_set_line_cap(cr, CAIRO_LINE_CAP_SQUARE);

				void (*mv)(cairo_t *, double, double);

				if (Ri[CORNER_TOP_LEFT])
				{
					cairo_move_to(cr, L, T + Ri[CORNER_TOP_LEFT]);
					cairo_arc(cr, L + Ri[CORNER_TOP_LEFT], T + Ri[CORNER_TOP_LEFT], Ri[CORNER_TOP_LEFT], PI, 1.5 * PI);
				}
				else cairo_move_to(cr, L, T);

				mv = s->border->edges[EDGE_TOP] ? &cairo_line_to : &cairo_move_to;
				if (Ri[CORNER_TOP_RIGHT])
				{
					mv(cr, L + W - Ri[CORNER_TOP_RIGHT], T);
					cairo_arc(cr, L + W - Ri[CORNER_TOP_RIGHT], T + Ri[CORNER_TOP_RIGHT], Ri[CORNER_TOP_RIGHT], 1.5 * PI, 2 * PI);
				}
				else mv(cr, L + W, T);

				mv = s->border->edges[EDGE_RIGHT] ? &cairo_line_to : &cairo_move_to;
				if (Ri[CORNER_BOTTOM_RIGHT])
				{
					mv(cr, L + W, T + H - Ri[CORNER_BOTTOM_RIGHT]);
					cairo_arc(cr, L + W - Ri[CORNER_BOTTOM_RIGHT], T + H - Ri[CORNER_BOTTOM_RIGHT], Ri[CORNER_BOTTOM_RIGHT], 0, 0.5 * PI);
				}
				else mv(cr, L + W, T + H);

				mv = s->border->edges[EDGE_BOTTOM] ? &cairo_line_to : &cairo_move_to;
				if (Ri[CORNER_BOTTOM_LEFT])
				{
					mv(cr, L + Ri[CORNER_BOTTOM_LEFT], T + H);
					cairo_arc(cr, L + Ri[CORNER_BOTTOM_LEFT], T + H - Ri[CORNER_BOTTOM_LEFT], Ri[CORNER_BOTTOM_LEFT], 0.5 * PI, PI);
				}
				else mv(cr, L, T + H);

				mv = s->border->edges[EDGE_LEFT] ? &cairo_line_to : &cairo_move_to;
				if (Ri[CORNER_TOP_LEFT])
				{
					mv(cr, L, T + Ri[CORNER_TOP_LEFT]);
				}
				else mv(cr, L, T);

				if (s->border->edges[EDGE_LEFT] && s->border->edges[EDGE_TOP])
					cairo_close_path(cr);

				cairo_stroke(cr);
			}

			color_free(bkc);
			color_free(bdc);

			for (i = 0; i < vector_size(s->children); ++i)
				element_paint((element *)vector_get(s->children, i), cr, debug);
			break;
		}
		case TYPE_TEXT:
		{
			text *t = (text *)e;
			if (t->color == NULL) break;
			FT_Face f = font_from_series(t->series);
			if (f == NULL) break;
			cairo_font_face_t *font = cairo_ft_font_face_create_for_ft_face(f, FT_LOAD_NO_HINTING);
			color *c = color_from_color_name(t->color);
			color_set_source(c, cr);
			cairo_set_font_face(cr, font);
			cairo_set_font_size(cr, 60);
			cairo_save(cr);
			cairo_translate(cr, t->geom->left - t->lsb, t->geom->top + t->size);
			cairo_scale(cr, t->scale, t->scale);
			cairo_move_to(cr, 0, 0);
			cairo_text_path(cr, t->data);
			cairo_fill(cr);
			cairo_restore(cr);
			cairo_font_face_destroy(font);
			color_free(c);
			break;
		}
		case TYPE_ARROW:
		{
			arrow *a = (arrow *)e;
			if (a->color == NULL) break;
			arrow_shape *arr = arrow_shape_create();
			arrow_read(a->name, arr);
			if (arr == NULL) break;
			double x1, y1, x2, y2, x, y;
			double t = a->angle * PI / 180.0;
			color *c = color_from_color_name(a->color);
			color_set_source(c, cr);

			x = a->geom->left;
			y = a->geom->top;
			cairo_save(cr);
			cairo_rotate(cr, t);
			path_draw(cr, arr->path);
			cairo_restore(cr);
			cairo_path_extents(cr, &x1, &y1, &x2, &y2);
			cairo_new_path(cr);

			cairo_matrix_t matrix;
			cairo_matrix_init(&matrix,
				a->scale * cos(t), sin(t), -sin(t), a->scale * cos(t), x - x1, y - y1);

			cairo_save(cr);
			cairo_transform(cr, &matrix);
			path_draw(cr, arr->path);
			cairo_fill(cr);
			cairo_restore(cr);
			color_free(c);
			break;
		}
		case TYPE_ICON:
		{
			icon *ic = (icon *)e;
			icon_shape *is = icon_shape_create();
			icon_read(ic->name, is);
			if (is == NULL) break;
			color *c = color_from_color_name(ic->color);
			cairo_save(cr);
			cairo_translate(cr, ic->geom->left, ic->geom->top);
			color_set_source(c, cr);
			cairo_save(cr);
			cairo_scale(cr, ic->scale, ic->scale);
			path_draw(cr, is->path);
			cairo_fill(cr);
			cairo_restore(cr);
			color_free(c);
			cairo_restore(cr);
			icon_shape_free(is);
			break;
		}
		case TYPE_ROUTE:
		{
			route *r = (route *)e;
			route_marker *rm = route_marker_create();
			route_read(r->marker, rm);
			if (rm == NULL) break;
			cairo_save(cr);
			cairo_translate(cr, r->geom->left, r->geom->top);
			cairo_scale(cr, r->scale, r->scale);
			int i;
			for (i = 0; i < rm->len; ++i)
			{
				color *c = color_from_color_name(rm->colors[i]);
				color_set_source(c, cr);
				path_draw(cr, rm->data[i]);
				cairo_save(cr);
				cairo_scale(cr, r->scale, r->scale);
				cairo_fill(cr);
				cairo_restore(cr);
				color_free(c);
			}
			for (i = 0; i < rm->texts; ++i)
			{
				FT_Face f = font_from_series(rm->series[i]);
				cairo_font_face_t *font = cairo_ft_font_face_create_for_ft_face(f, FT_LOAD_NO_HINTING);
				color *c = color_from_color_name(rm->textcolors[i]);
				color_set_source(c, cr);
				cairo_set_font_face(cr, font);
				cairo_set_font_size(cr, 60);
				cairo_text_extents_t ext;
				cairo_text_extents(cr, "A", &ext);
				double scale = rm->sizes[i] / ext.height;
				char *text = strcmp(rm->text[i], "#") ? rm->text[i] : r->number;
				cairo_text_extents(cr, text, &ext);
				double textleft = rm->xs[i] - ext.width * scale / 2;
				double texttop = rm->ys[i] - rm->sizes[i];
				double lsb = ext.x_bearing * scale;
				cairo_move_to(cr, textleft - lsb, texttop + rm->sizes[i]);
				cairo_save(cr);
				cairo_scale(cr, scale, scale);
				cairo_text_path(cr, text);
				cairo_fill(cr);
				cairo_restore(cr);
				cairo_fill(cr);
				cairo_font_face_destroy(font);
				color_free(c);
			}
			cairo_restore(cr);
			route_marker_free(rm);
			break;
		}
	}

	if (debug)
	{
		color *red = color_from_color_name("red");

		color_set_source(red, cr);
		cairo_set_line_width(cr, 0.1);
		cairo_rectangle(cr, e->geom->left, e->geom->top,
		                    e->geom->width, e->geom->height);
		cairo_stroke(cr);

		color_set_source(red, cr);
		cairo_set_line_width(cr, 0.1);
		cairo_rectangle(cr, e->geom->left - e->margin[EDGE_LEFT], e->geom->top - e->margin[EDGE_TOP],
		                    e->geom->width + e->margin[EDGE_LEFT] + e->margin[EDGE_RIGHT], e->geom->height + e->margin[EDGE_TOP] + e->margin[EDGE_BOTTOM]);
		cairo_stroke(cr);

		color_free(red);
	}
}
