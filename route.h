#ifndef sfrsm_route_h
#define sfrsm_route_h

typedef struct route_marker_
{
	int len;
	char **colors;
	char **data;
	int texts;
	double *xs;
	double *ys;
	double *sizes;
	char **series;
	char **text;
	char **textcolors;
} route_marker;

route_marker *route_marker_create();
void route_read(char *name, route_marker *r);
void route_marker_free(route_marker *rm);

#endif
