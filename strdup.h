#ifndef sfrsm_strdup_h
#define sfrsm_strdup_h

#include <string.h>

#ifndef strdup
char *strdup(const char *s);
#endif /* strdup */

#endif /* sfrsm_strdup_h */
