#ifndef sfrsm_build_h
#define sfrsm_build_h

#include <stdio.h>
#include "json.h"
#include "librsm.h"

stack *parse_file(FILE *f);

#endif /* sfrsm_build_h */
