#include "json.h"
#include "librsm.h"
#include "parser.h"

static text *build_text(json_value *val)
{
	if (val == NULL) return NULL;
	if (val->type != json_object) return NULL;

	char *color, *series, *data;
	double size;

	color = parse_string(json_value_find_key(val, "color"));
	series = parse_string(json_value_find_key(val, "series"));
	data = parse_string(json_value_find_key(val, "text"));
	size = parse_number(json_value_find_key(val, "size"));

	text *t = text_create(size, color, series, data);

	t->margin = parse_four_numbers(json_value_find_key(val, "margin"));
	t->halign = parse_halign(json_value_find_key(val, "halign"));
	t->valign = parse_valign(json_value_find_key(val, "valign"));

	return t;
}

static arrow *build_arrow(json_value *val)
{
	char *name, *color;

	color = parse_string(json_value_find_key(val, "color"));
	name = parse_string(json_value_find_key(val, "arrow"));

	arrow *a = arrow_create(name, color);

	a->angle = parse_number(json_value_find_key(val, "angle"));

	a->width = parse_number(json_value_find_key(val, "width"));
	a->height = parse_number(json_value_find_key(val, "height"));
	a->margin = parse_four_numbers(json_value_find_key(val, "margin"));
	a->halign = parse_halign(json_value_find_key(val, "halign"));
	a->valign = parse_valign(json_value_find_key(val, "valign"));

	return a;
}

static icon *build_icon(json_value *val)
{
	char *name, *color;

	color = parse_string(json_value_find_key(val, "color"));
	name = parse_string(json_value_find_key(val, "icon"));

	icon *i = icon_create(name, color);

	i->width = parse_number(json_value_find_key(val, "width"));
	i->height = parse_number(json_value_find_key(val, "height"));
	i->margin = parse_four_numbers(json_value_find_key(val, "margin"));
	i->halign = parse_halign(json_value_find_key(val, "halign"));
	i->valign = parse_valign(json_value_find_key(val, "valign"));

	return i;
}

static route *build_route(json_value *val)
{
	char *marker, *number;

	marker = parse_string(json_value_find_key(val, "marker"));
	number = parse_string(json_value_find_key(val, "route"));

	route *r = route_create(marker, number);

	r->width = parse_number(json_value_find_key(val, "width"));
	r->height = parse_number(json_value_find_key(val, "height"));
	r->margin = parse_four_numbers(json_value_find_key(val, "margin"));
	r->halign = parse_halign(json_value_find_key(val, "halign"));
	r->valign = parse_valign(json_value_find_key(val, "valign"));

	return r;
}

static stack *build_stack(json_value *val)
{
	if (val == NULL) return NULL;
	if (val->type != json_object) return NULL;

	type_enum type;
	if (json_value_has_key(val, "xstack")) type = TYPE_XSTACK;
	else if (json_value_has_key(val, "ystack")) type = TYPE_YSTACK;
	else if (json_value_has_key(val, "zstack")) type = TYPE_ZSTACK;
	else return NULL;

	stack *st = stack_create(type);

	int i;

	st->width = parse_number(json_value_find_key(val, "width"));
	st->height = parse_number(json_value_find_key(val, "height"));
	st->margin = parse_four_numbers(json_value_find_key(val, "margin"));
	st->halign = parse_halign(json_value_find_key(val, "halign"));
	st->valign = parse_valign(json_value_find_key(val, "valign"));

	st->backcolor = parse_string(json_value_find_key(val, "backcolor"));
	st->padding = parse_four_numbers(json_value_find_key(val, "padding"));

	st->border->width = parse_number(json_value_find_key(val, "border"));
	st->border->color = parse_string(json_value_find_key(val, "bordercolor"));
	st->border->gap = parse_number(json_value_find_key(val, "bordergap"));
	st->border->edges = parse_four_booleans(json_value_find_key(val, "borderon"));
	for (i = 0; i < 4; ++i)
		if (st->border->edges[i] == -1)
			st->border->edges[i] = 1;

	st->corner->radius = parse_four_numbers(json_value_find_key(val, "radius"));
	st->corner->rounding = parse_four_booleans(json_value_find_key(val, "rounding"));
	for (i = 0; i < 4; ++i)
		if (st->corner->rounding[i] == -1)
			st->corner->rounding[i] = 1;

	json_value *ch;
	if (json_value_has_key(val, "xstack")) ch = json_value_find_key(val, "xstack");
	else if (json_value_has_key(val, "ystack")) ch = json_value_find_key(val, "ystack");
	else ch = json_value_find_key(val, "zstack");
	if (ch == NULL) return st;
	if (ch->type != json_array) return st;
	for (i = 0; i < ch->u.array.length; ++i)
	{
		json_value *child = ch->u.array.values[i];

		if (json_value_has_key(child, "xstack") ||
			json_value_has_key(child, "ystack") ||
			json_value_has_key(child, "zstack"))
			stack_add_child(st, (element *)build_stack(child));
		else if (json_value_has_key(child, "text"))
			stack_add_child(st, (element *)build_text(child));
		else if (json_value_has_key(child, "arrow"))
			stack_add_child(st, (element *)build_arrow(child));
		else if (json_value_has_key(child, "icon"))
			stack_add_child(st, (element *)build_icon(child));
		else if (json_value_has_key(child, "route"))
			stack_add_child(st, (element *)build_route(child));
	}

	return st;
}

stack *parse_file(FILE *f)
{
	json_value *val = read_json_file(f);
	stack *s = build_stack(val);
	json_value_free(val);
	return s;
}
