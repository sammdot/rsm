#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "json.h"
#include "librsm.h"
#include "strdup.h"

json_value *read_json_file(FILE *f)
{
	if (f == NULL) return NULL;

	char *buf, err[128];
	int size = 0;

	fseek(f, 0L, SEEK_END);
	size = ftell(f);
	fseek(f, 0L, SEEK_SET);
	buf = malloc(size + 1);

	int siz = fread(buf, 1, size, f);
	buf[siz] = 0;

	json_value *val;
	json_settings settings = { 0 };
	settings.settings |= json_enable_comments;

	val = json_parse_ex(&settings, buf, strlen(buf), err);

	if (val == NULL)
		fprintf(stderr, "rsm: %s\n", err);

	return val;
}

double parse_number(json_value *val)
{
	if (val == NULL) return 0.0;
	if (val->type == json_integer) return (double)val->u.integer;
	else if (val->type == json_double) return val->u.dbl;
	else if (val->type == json_string) return strtod(val->u.string.ptr, NULL);
	else return 0.0;
}

double *parse_four_numbers(json_value *val)
{
	if (val == NULL)
	{
		double *d = malloc(4 * sizeof(double));
		int i;
		for (i = 0; i < 4; ++i)
			d[i] = 0.0;
		return d;
	}
	if (val->type != json_array)
	{
		double *d = malloc(4 * sizeof(double));
		int i;
		for (i = 0; i < 4; ++i)
			d[i] = 0.0;
		return d;
	}

	double *d = malloc(4 * sizeof(double));
	int i;
	for (i = 0; i < 4; ++i)
	{
		if (i > val->u.array.length)
		{
			d[i] = 0.0;
			continue;
		}
		json_value *k = val->u.array.values[i];
		d[i] = parse_number(k);
	}

	return d;
}

char *parse_string(json_value *val)
{
	if (val == NULL) return "";

	if (val->type == json_string)
		return strdup(val->u.string.ptr);
	else if (val->type == json_integer || val->type == json_double)
	{
		char v[16];
		if (val->type == json_integer)
			sprintf(v, "%d", (int)val->u.integer);
		else
		{
			sprintf(v, "%f", val->u.dbl);
			int i, j;
			for (i = 0; i < strlen(v); ++i)
				if (v[i] == '.') break;
			for (j = i; j < strlen(v); ++j)
				if (v[j] == '0') break;
			if (j == i) v[j - 1] = '\0';
			else v[j] = '\0';
		}
		return strdup(v);
	}
	return "";
}

int parse_boolean(json_value *val)
{
	if (val == NULL) return -1;
	if (val->type == json_string)
	{
		char *v = val->u.string.ptr;
		if (!strcmp(v, "true")) return 1;
		else if (!strcmp(v, "false")) return 0;
	}
	else if (val->type == json_integer)
		return val->u.integer;
	else if (val->type == json_double)
		return val->u.dbl;
	else if (val->type != json_boolean) return 0;
	return val->u.boolean;
}

int *parse_four_booleans(json_value *val)
{
	if (val == NULL)
	{
		int *d = malloc(4 * sizeof(int));
		int i;
		for (i = 0; i < 4; ++i)
			d[i] = -1;
		return d;
	}
	if (val->type != json_array)
	{
		int *d = malloc(4 * sizeof(int));
		int i;
		for (i = 0; i < 4; ++i)
			d[i] = -1;
		return d;
	}

	int *d = malloc(4 * sizeof(int));
	int i;
	for (i = 0; i < 4; ++i)
	{
		if (i > val->u.array.length)
		{
			d[i] = -1;
			continue;
		}
		json_value *k = val->u.array.values[i];
		d[i] = parse_boolean(k);
	}

	return d;
}

halign_enum parse_halign(json_value *val)
{
	char *hal = parse_string(val);
	return !strcmp(hal, "left") ? HALIGN_LEFT :
			!strcmp(hal, "center") ? HALIGN_CENTER :
			!strcmp(hal, "right") ? HALIGN_RIGHT :
			!strcmp(hal, "full") ? HALIGN_FULL : HALIGN_CENTER;
}

valign_enum parse_valign(json_value *val)
{
	char *vtal = parse_string(val);
	return !strcmp(vtal, "top") ? VALIGN_TOP :
			!strcmp(vtal, "middle") ? VALIGN_MIDDLE :
			!strcmp(vtal, "bottom") ? VALIGN_BOTTOM :
			!strcmp(vtal, "full") ? VALIGN_FULL : VALIGN_MIDDLE;
}

json_value *json_value_find_key(json_value *val, char *key)
{
	if (val == NULL || key == NULL) return NULL;
	if (val->type != json_object) return NULL;
	int i;
	for (i = 0; i < val->u.object.length; ++i)
		if (!strcmp(key, val->u.object.values[i].name))
			return val->u.object.values[i].value;
	return NULL;
}

int json_value_has_key(json_value *val, char *key)
{
	return json_value_find_key(val, key) != NULL;
}
