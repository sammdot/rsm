#include <stdlib.h>
#include <string.h>

#include "vector.h"

vector *vector_init()
{
	vector *v = malloc(sizeof(vector));
	v->data = malloc(sizeof(void *));
	v->size = 0;
	v->limit = 1;
	return v;
}

void vector_free(vector *v)
{
	if (v == NULL) return;
	free(v->data);
	free(v);
}

int vector_size(vector *v)
{
	if (v == NULL) return -1;
	return v->size;
}

void vector_push(vector *v, int i, void *e)
{
	if (v == NULL) return;
	if (v->size + 1 == v->limit)
	{
		void **t = realloc(v->data, (v->limit + 1) * sizeof(void *));
		if (!t) return;
		v->data = t;
		++v->limit;
	}
	int j;
	for (j = v->size; j > i; --i)
		v->data[j] = v->data[j - 1];
	v->data[i] = e;
	++v->size;
}

void vector_push_back(vector *v, void *e)
{
	if (v == NULL) return;
	vector_push(v, vector_size(v), e);
}

void *vector_get(vector *v, int i)
{
	if (v == NULL) return NULL;
	if (i >= vector_size(v)) return NULL;
	return v->data[i];
}

void vector_remove(vector *v, int i)
{
	if (v == NULL) return;
	int j;
	for (j = i; i < v->size; ++i)
		v->data[j] = v->data[j + 1];
	--v->size;
}
