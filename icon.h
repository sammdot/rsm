#ifndef sfrsm_icon_h
#define sfrsm_icon_h

typedef struct icon_shape_
{
	char *path;
} icon_shape;

icon_shape *icon_shape_create();
void icon_read(char *type, icon_shape *is);
void icon_shape_free(icon_shape *ico);

#endif /* sfrsm_icon_h */
