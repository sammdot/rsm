#include <stdio.h>
#include <string.h>
#include "dir.h"
#include "strdup.h"

char *slash(char *path)
{
	if (strlen(path) == 0) return "";
	char *p = strdup(path);
	int i;
	for (i = 0; i < strlen(p); ++i)
		if (p[i] == '\\') p[i] = '/';
	return p;
}

char *dirname(char *path)
{
	if (strlen(path) == 0) return "";
	char *p = strdup(path);
	int i;
	for (i = strlen(p) - 1; i >= 0; --i)
		if (p[i] == '/') break;
	p[i] = '\0';
	return p;
}

#ifdef WIN32

#include <windows.h>

char *getappdir(void)
{
	char *ch, d[256];
	GetModuleFileName(NULL, d, 256);
	ch = slash(d);
	return dirname(ch);
}

#else

#include <unistd.h>
int readlink(const char *path, char *buf, size_t bufsiz);

char *getappdir(void)
{
	char *ch, d[256];
	readlink("/proc/self/exe", d, 256);
	ch = strdup(d);
	return dirname(ch);
}

#endif /* WIN32 */
