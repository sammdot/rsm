#ifndef sfrsm_parser_h
#define sfrsm_parser_h

#include <stdio.h>
#include "json.h"
#include "librsm.h"

json_value *read_json_file(FILE *f);
double parse_number(json_value *val);
double *parse_four_numbers(json_value *val);
char *parse_string(json_value *val);
int parse_boolean(json_value *val);
int *parse_four_booleans(json_value *val);
halign_enum parse_halign(json_value *val);
valign_enum parse_valign(json_value *val);
json_value *json_value_find_key(json_value *val, char *key);
int json_value_has_key(json_value *val, char *key);

#endif
