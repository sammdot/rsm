#ifndef sfrsm_arrow_h
#define sfrsm_arrow_h

typedef struct arrow_shape_
{
	char *path;
} arrow_shape;

arrow_shape *arrow_shape_create();
void arrow_read(char *type, arrow_shape *arr);
void arrow_shape_free(arrow_shape *arr);

#endif /* sfrsm_arrow_h */
