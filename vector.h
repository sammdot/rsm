#ifndef sfrsm_vector_h
#define sfrsm_vector_h

typedef struct vector_
{
	void **data;
	int size;
	int limit;
} vector;

vector *vector_init(void);
void vector_free(vector *v);
int vector_size(vector *v);
void vector_push(vector *v, int i, void *e);
void vector_push_back(vector *v, void *e);
void *vector_get(vector *v, int i);
void vector_remove(vector *v, int i);

#endif /* sfrsm_vector_h */
