#ifndef sfrsm_getopt_h
#define sfrsm_getopt_h

extern int opterr, optind, optopt, optreset;
extern char *optarg;

int getopt_(int argc, char * const *argv, const char *ostr);

#endif
