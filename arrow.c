#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arrow.h"
#include "dir.h"
#include "json.h"
#include "parser.h"
#include "strdup.h"

extern char *dir;

arrow_shape *arrow_shape_create()
{
	arrow_shape *arr = malloc(sizeof(arrow_shape));
	arr->path = malloc(256 * sizeof(char));
	return arr;
}

void arrow_read(char *type, arrow_shape *arr)
{
	char fn[64];
	sprintf(fn, "%s/arrows/%s.arr", dir, type);

	FILE *f = fopen(fn, "r");

	if (f == NULL)
	{
		fprintf(stderr, "rsm: unrecognized arrow type '%s'\n", type);
		arrow_shape_free(arr);
		exit(1);
	}

	json_value *val = read_json_file(f);
	arr->path = parse_string(json_value_find_key(val, "path"));
	fclose(f);
}

void arrow_shape_free(arrow_shape *arr)
{
	free(arr->path);
	free(arr);
}
