#ifndef sfrsm_path_h
#define sfrsm_path_h

#include <cairo.h>
#include "vector.h"

void path_draw(cairo_t *cr, char *d);

#endif /* sfrsm_path_h */
