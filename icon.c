#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "path.h"
#include "icon.h"
#include "dir.h"
#include "json.h"
#include "parser.h"
#include "strdup.h"

extern char *dir;

icon_shape *icon_shape_create()
{
	icon_shape *i = malloc(sizeof(icon_shape));
	i->path = malloc(16384 * sizeof(char));
	return i;
}

void icon_read(char *type, icon_shape *is)
{
	char fn[80];
	sprintf(fn, "%s/icons/%s.icn", dir, type);

	FILE *f = fopen(fn, "r");

	if (f == NULL)
	{
		fprintf(stderr, "rsm: unrecognized icon type '%s'\n", type);
		icon_shape_free(is);
		exit(1);
	}

	json_value *val = read_json_file(f);
	is->path = parse_string(json_value_find_key(val, "path"));
	fclose(f);
}

void icon_shape_free(icon_shape *i)
{
	free(i->path);
	free(i);
}
