#ifndef sfrsm_render_h
#define sfrsm_render_h

#include <cairo.h>
#include <cairo-ft.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include "librsm.h"

void element_dimension(element *e);
void element_position(element *e, double left, double top);
void element_paint(element *e, cairo_t *cr, int debug);

#endif
