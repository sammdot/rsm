#include <string.h>
#include <stdio.h>
#include "getopt.h"

int opterr,
	optind,
	optopt,
	optreset;
char *optarg;

int getopt_(int argc, char * const *argv, const char *ostr)
{
	static char *place = "";
	char *oli;

	if (optreset || !*place)
	{
		optreset = 0;
		if (optind >= argc || *(place = argv[optind]) != '-')
		{
			place = "";
			return -1;
		}
		if (place[1] && *++place == '-')
		{
			++optind;
			place = "";
			return -1;
		}
	}

	if ((optopt = *place++) == ':' ||
	    !(oli = strchr(ostr, optopt)))
	{
		if (optopt == '-')
			return -1;
		if (!*place)
			++optind;
		if (opterr && *ostr != ':')
			printf("rsm: invalid option -- %c\n", optopt);
		return '?';
	}

	if (*++oli != ':')
	{
		optarg = NULL;
		if (!*place)
			++optind;
	}
	else
	{
		if (*place)
			optarg = place;
		else if (argc <= ++optind)
		{
			place = "";
			if (*ostr == ':')
				return ':';
			if (opterr)
				printf("rsm: option requires an argument -- %c\n", optopt);
			return '?';
		}
		else
			optarg = argv[optind];
		place = "";
		++optind;
	}
	return optopt;
}
