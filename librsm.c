#include <stdlib.h>
#include <string.h>
#include "librsm.h"
#include "vector.h"
#include "dir.h"
#include "strdup.h"

#include <cairo.h>
#include <cairo-ft.h>
#include <ft2build.h>
#include FT_FREETYPE_H

geometry *geometry_create(void)
{
	geometry *g = malloc(sizeof(geometry));
	g->width = 0;
	g->height = 0;
	g->left = 0;
	g->top = 0;
	return g;
}

void geometry_free(geometry *g)
{
	free(g);
}

color *color_from_rgba(int r, int g, int b, int a)
{
	color *c = malloc(sizeof(color));
	c->red = (r > 255) ? 1.0 : (float)r / 255.0;
	c->green = (g > 255) ? 1.0 : (float)g / 255.0;
	c->blue = (b > 255) ? 1.0 : (float)b / 255.0;
	c->alpha = (a > 100) ? 1.0 : (float)a / 100.0;
	return c;
}

color *color_from_rgb(int r, int g, int b)
{
	return color_from_rgba(r, g, b, 100);
}

color *color_from_hex(char *hex)
{
	if (strlen(hex) != 6) return NULL;
	long int h = strtol(hex, NULL, 16);
	return color_from_rgb(h >> 16 & 0xFF, h >> 8 & 0xFF, h & 0xFF);
}

color *color_from_color_name(char *name)
{
	if (!strcmp(name, "white")) return color_from_rgb(251, 255, 237);
	else if (!strcmp(name, "red")) return color_from_rgb(196, 0, 39);
	else if (!strcmp(name, "yellow")) return color_from_rgb(255, 181, 0);
	else if (!strcmp(name, "green")) return color_from_rgb(0, 95, 71);
	else if (!strcmp(name, "blue")) return color_from_rgb(0, 67, 114);
	else if (!strcmp(name, "black")) return color_from_rgb(51, 51, 51);
	return color_from_hex(name);
}

void color_set_source(color *c, cairo_t *cr)
{
	if (c == NULL)
		cairo_set_source_rgba(cr, 0, 0, 0, 0);
	else
		cairo_set_source_rgba(cr, c->red, c->green, c->blue, c->alpha);
}

void color_free(color *c)
{
	free(c);
}

FT_Face font_from_series(char *name)
{
	if (!lib) return NULL;
	char fname[80];
	sprintf(fname, "%s/fonts/RSM%s.ttf", dir, name);
	FT_Face f;
	FT_New_Face(lib, fname, 0, &f);
	if (f == NULL)
		fprintf(stderr, "rsm: unrecognized font series '%s'\n", name);
	return f;
}

border *border_create(void)
{
	border *b = malloc(sizeof(border));
	b->width = 0;
	b->color = "";
	b->gap = 0;
	b->edges = malloc(4 * sizeof(int));
	int i;
	for (i = 0; i < 4; ++i)
		b->edges[i] = 1;
	return b;
}

void border_free(border *b)
{
	free(b->edges);
	free(b);
}

corner *corner_create(void)
{
	corner *c = malloc(sizeof(corner));
	c->radius = malloc(4 * sizeof(double));
	c->rounding = malloc(4 * sizeof(int));
	int i;
	for (i = 0; i < 4; ++i)
	{
		c->radius[i] = 0;
		c->rounding[i] = 1;
	}
	return c;
}

void corner_free(corner *c)
{
	free(c->radius);
	free(c->rounding);
	free(c);
}

stack *stack_create(type_enum t)
{
	stack *s = (stack *)malloc(sizeof(stack));
	s->type_ = t;
	s->geom = geometry_create();
	s->margin = malloc(4 * sizeof(double));
	s->halign = HALIGN_CENTER;
	s->valign = VALIGN_MIDDLE;
	s->width = 0;
	s->height = 0;
	s->inwidth = 0;
	s->inheight = 0;
	s->backcolor = "";
	s->padding = malloc(4 * sizeof(double));
	s->border = border_create();
	s->corner = corner_create();
	s->children = vector_init();
	int i;
	for (i = 0; i < 4; ++i)
	{
		s->margin[i] = 0;
		s->padding[i] = 0;
	}
	return s;
}

stack *stack_create_sub(stack *parent, type_enum t)
{
	stack *s = stack_create(t);
	stack_add_child(parent, (element *)s);
	return s;
}

void stack_add_child(stack *s, element *e)
{
	if (s == NULL) return;
	e->parent = s;
	vector_push_back(s->children, e);
}

void stack_free(stack *s)
{
	free(s->geom);
	free(s->margin);
	free(s->padding);
	border_free(s->border);
	corner_free(s->corner);
	vector_free(s->children);
	free(s);
}

text *text_create(double size, char *color, char *series, char *data)
{
	text *t = malloc(sizeof(text));
	t->type_ = TYPE_TEXT;
	t->geom = geometry_create();
	t->margin = malloc(4 * sizeof(double));
	t->halign = HALIGN_CENTER;
	t->valign = VALIGN_MIDDLE;
	t->scale = 1.0;
	t->size = size;
	t->color = strdup(color);
	t->series = strdup(series);
	t->data = strdup(data);
	int i;
	for (i = 0; i < 4; ++i)
		t->margin[i] = 0;
	return t;
}

text *text_create_sub(stack *parent, double size, char *color,
                 char *series, char *data)
{
	text *t = text_create(size, color, series, data);
	stack_add_child(parent, (element *)t);
	return t;
}

void text_free(text *t)
{
	free(t->geom);
	free(t->margin);
	free(t->data);
	free(t);
}

arrow *arrow_create(char *name, char *color)
{
	arrow *a = malloc(sizeof(arrow));
	a->type_ = TYPE_ARROW;
	a->geom = geometry_create();
	a->margin = malloc(4 * sizeof(double));
	a->halign = HALIGN_CENTER;
	a->valign = VALIGN_MIDDLE;
	a->width = 0;
	a->height = 0;
	a->scale = 1.0;
	a->name = strdup(name);
	a->color = strdup(color);
	a->angle = 0;
	int i;
	for (i = 0; i < 4; ++i)
		a->margin[i] = 0;
	return a;
}

arrow *arrow_create_sub(stack *parent, char *name, char *color)
{
	arrow *a = arrow_create(name, color);
	stack_add_child(parent, (element *)a);
	return a;
}

void arrow_free(arrow *a)
{
	free(a->geom);
	free(a->margin);
	free(a->name);
	free(a);
}

icon *icon_create(char *name, char *color)
{
	icon *i = malloc(sizeof(icon));
	i->type_ = TYPE_ICON;
	i->geom = geometry_create();
	i->margin = malloc(4 * sizeof(double));
	i->halign = HALIGN_CENTER;
	i->valign = VALIGN_MIDDLE;
	i->width = 0;
	i->height = 0;
	i->scale = 1.0;
	i->name = strdup(name);
	i->color = strdup(color);
	int j;
	for (j = 0; j < 4; ++j)
		i->margin[j] = 0;
	return i;
}

icon *icon_create_sub(stack *parent, char *name, char *color)
{
	icon *i = icon_create(name, color);
	stack_add_child(parent, (element *)i);
	return i;
}

void icon_free(icon *i)
{
	free(i->geom);
	free(i->margin);
	free(i->name);
	free(i);
}

route *route_create(char *marker, char *number)
{
	route *r = malloc(sizeof(route));
	r->type_ = TYPE_ROUTE;
	r->geom = geometry_create();
	r->margin = malloc(4 * sizeof(double));
	r->halign = HALIGN_CENTER;
	r->valign = VALIGN_MIDDLE;
	r->width = 0;
	r->height = 0;
	r->scale = 1.0;
	r->marker = strdup(marker);
	r->number = strdup(number);
	int i;
	for (i = 0; i < 4; ++i)
		r->margin[i] = 0;
	return r;
}

route *route_create_sub(stack *parent, char *marker, char *number)
{
	route *r = route_create(marker, number);
	stack_add_child(parent, (element *)r);
	return r;
}

void route_free(route *r)
{
	free(r->geom);
	free(r->margin);
	free(r->marker);
	free(r->number);
}
