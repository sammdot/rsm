#ifndef sfrsm_librsm_h
#define sfrsm_librsm_h

#include <stdlib.h>
#include <string.h>
#include "vector.h"

#include <cairo.h>
#include <cairo-ft.h>
#include <ft2build.h>
#include FT_FREETYPE_H

typedef enum type_enum_
{
	TYPE_XSTACK = 0,
	TYPE_YSTACK,
	TYPE_ZSTACK,
	TYPE_TEXT,
	TYPE_ARROW,
	TYPE_ICON,
	TYPE_ROUTE
} type_enum;

typedef enum halign_enum_
{
	HALIGN_LEFT = 0,
	HALIGN_CENTER,
	HALIGN_RIGHT,
	HALIGN_FULL
} halign_enum;

typedef enum valign_enum_
{
	VALIGN_TOP = 0,
	VALIGN_MIDDLE,
	VALIGN_BOTTOM,
	VALIGN_FULL
} valign_enum;

typedef enum edge_enum_
{
	EDGE_LEFT = 0,
	EDGE_TOP,
	EDGE_RIGHT,
	EDGE_BOTTOM
} edge_enum;

typedef enum corner_enum_
{
	CORNER_TOP_LEFT = 0,
	CORNER_TOP_RIGHT,
	CORNER_BOTTOM_RIGHT,
	CORNER_BOTTOM_LEFT
} corner_enum;

typedef struct geometry_
{
	double width;
	double height;
	double left;
	double top;
} geometry;

typedef struct color_
{
	double red;
	double green;
	double blue;
	double alpha;
} color;

typedef struct border_
{
	double width;
	char *color;
	int *edges;
	double gap;
} border;

typedef struct corner_
{
	double *radius;
	int *rounding;
} corner;

struct stack_;

typedef struct element_
{
	type_enum type_;
	struct stack_ *parent;
	geometry *geom;
	double *margin;
	halign_enum halign;
	valign_enum valign;
	double width;
	double height;
} element;

typedef struct stack_
{
	type_enum type_;
	struct stack_ *parent;
	geometry *geom;
	double *margin;
	halign_enum halign;
	valign_enum valign;
	double width;
	double height;

	double inwidth;
	double inheight;
	char *backcolor;
	double *padding;
	border *border;
	corner *corner;
	vector *children;
} stack;

typedef struct text_
{
	type_enum type_;
	stack *parent;
	geometry *geom;
	double *margin;
	halign_enum halign;
	valign_enum valign;
	double width;
	double height;

	double lsb;
	double scale;
	double size;
	char *color;
	char *series;
	char *data;
} text;

typedef struct arrow_
{
	type_enum type_;
	stack *parent;
	geometry *geom;
	double *margin;
	halign_enum halign;
	valign_enum valign;
	double width;
	double height;

	double scale;
	char *name;
	char *color;
	double angle;
} arrow;

typedef struct icon_
{
	type_enum type_;
	stack *parent;
	geometry *geom;
	double *margin;
	halign_enum halign;
	valign_enum valign;
	double width;
	double height;

	double scale;
	char *name;
	char *color;
} icon;

typedef struct route_
{
	type_enum type_;
	stack *parent;
	geometry *geom;
	double *margin;
	halign_enum halign;
	valign_enum valign;
	double width;
	double height;

	double scale;
	char *marker;
	char *number;
} route;

FT_Library lib;

geometry *geometry_create(void);
void geometry_free(geometry *g);

color *color_from_rgba(int r, int g, int b, int a);
color *color_from_rgb(int r, int g, int b);
color *color_from_hex(char *hex);
color *color_from_color_name(char *name);
void color_set_source(color *c, cairo_t *cr);
void color_free(color *c);

FT_Face font_from_series(char *name);

border *border_create(void);
void border_free(border *b);

corner *corner_create(void);
void corner_free(corner *c);

stack *stack_create(type_enum t);
stack *stack_create_sub(stack *parent, type_enum t);
void stack_add_child(stack *s, element *child);
void stack_remove_child(stack *s, int i);
void stack_free(stack *s);

text *text_create(double size, char *color, char *series, char *data);
text *text_create_sub(stack *parent, double size, char *color,
                 char *series, char *data);
void text_free(text *tl);

arrow *arrow_create(char *type, char *color);
arrow *arrow_create_sub(stack *parent, char *type, char *color);
void arrow_free(arrow *a);

icon *icon_create(char *type, char *color);
icon *icon_create_sub(stack *parent, char *type, char *color);
void icon_free(icon *i);

route *route_create(char *marker, char *number);
route *route_create_sub(stack *parent, char *marker, char *number);
void route_free(route *r);

#endif /* sfrsm_librsm_h */
