#ifndef sfrsm_dir_h
#define sfrsm_dir_h

char *dir;

char *slash(char *path);
char *dirname(char *path);

char *getappdir();

#endif /* sfrsm_dir_h */
