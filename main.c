#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cairo.h>
#include <cairo-ft.h>
#include <cairo-pdf.h>
#include <cairo-svg.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include "build.h"
#include "dir.h"
#include "getopt.h"
#include "librsm.h"
#include "parser.h"
#include "render.h"
#include "strdup.h"

extern FT_Library lib;
extern char *dir;

char *usage = "usage:\n\
    rsm [-V] [-v] [-d] [-s SCALE] [-f FORMAT] < JSONFILE > OUTFILE\n";
const char *ver = "rsm: road sign maker %s\n";

#define rsm_major_version  1
#define rsm_minor_version  0

char *rsm_version(void)
{
	char vs[10];
	sprintf(vs, "%d.%d", rsm_major_version, rsm_minor_version);
	return strdup(vs);
}

cairo_status_t write_to_stream(void /* FILE */ *fp, const unsigned char *data, unsigned int length)
{
	if (fp == NULL) return CAIRO_STATUS_WRITE_ERROR;
	size_t s = fwrite(data, 1, length, (FILE *)fp);
	return s == length ? CAIRO_STATUS_SUCCESS : CAIRO_STATUS_WRITE_ERROR;
}

int main(int argc, char **argv)
{
	double scale = 1.0;
	int debug = 0, verbose = 0;

	int opt;
	char *fmt = "svg";

	while ((opt = getopt_(argc, argv, "Vvds:f:")) != -1)
	{
		switch (opt)
		{
			case 'V':
				fprintf(stderr, ver, rsm_version());
				return 0;
			case 'd':
				debug = 1;
				break;
			case 'v':
				verbose = 1;
				break;
			case 's':
				scale = strtod(optarg, NULL);
				break;
			case 'f':
				fmt = strdup(optarg);
				if (strcmp(fmt, "png") && strcmp(fmt, "svg"))
				{
					fprintf(stderr, "rsm: unrecognized format: %s\n", fmt);
					return 1;
				}
				break;
			default:
				fputs(usage, stderr);
				return 1;
		}
	}

	if (verbose && debug)
		fprintf(stderr, "rsm: debug mode enabled\n");

	dir = getappdir();
	if (verbose)
		fprintf(stderr, "rsm: program folder: %s\nrsm: loading objects\n", dir);

	FT_Init_FreeType(&lib);

	if (verbose) fprintf(stderr, "rsm: parsing input\n");
	element *sign = (element *)parse_file(stdin);
	if (sign == NULL)
	{
		fprintf(stderr, "rsm: parsing failed\n");
		return 1;
	}

	if (verbose)
		fprintf(stderr, "rsm: calculating sign layout\n");

	element_dimension(sign);
	element_position(sign, 0, 0);

	double w, h;

	w = scale * (sign->geom->width + sign->margin[EDGE_LEFT] + sign->margin[EDGE_RIGHT]);
	h = scale * (sign->geom->height + sign->margin[EDGE_TOP] + sign->margin[EDGE_BOTTOM]);

	if (verbose)
	{
		fprintf(stderr, "rsm: sign dimensions: %0.2fx%0.2f (scaled: %0.0fx%0.0f)\n",
	        sign->geom->width, sign->geom->height,
	        ceil(scale * sign->geom->width), ceil(scale * sign->geom->height));
		fprintf(stderr, "rsm: image dimensions: %0.2fx%0.2f (scaled: %0.0fx%0.0f)\n",
	        w / scale, h / scale, ceil(w), ceil(h));
	}

	w = ceil(w);
	h = ceil(h);

	cairo_surface_t *img;
	if (!strcmp(fmt, "svg"))
		img = cairo_svg_surface_create_for_stream(write_to_stream, stdout, w, h);
	else
		img = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, w, h);

	cairo_t *cr = cairo_create(img);
	cairo_set_fill_rule(cr, CAIRO_FILL_RULE_EVEN_ODD);

	if (verbose)
		fprintf(stderr, "rsm: painting sign\n");
	cairo_save(cr);
	cairo_scale(cr, scale, scale);
	element_paint(sign, cr, debug);
	cairo_restore(cr);

	if (!strcmp(fmt, "png"))
	{
		if (verbose)
			fprintf(stderr, "rsm: writing to PNG\n");
		cairo_surface_write_to_png_stream(img, write_to_stream, stdout);
	}

	if (verbose) fprintf(stderr, "rsm: all done\n");

	cairo_destroy(cr);
	cairo_surface_finish(img);
	cairo_surface_destroy(img);

	FT_Done_FreeType(lib);

	return 0;
}
