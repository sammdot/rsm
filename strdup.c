#include <stdlib.h>
#include <string.h>

#ifndef strdup

#include "strdup.h"

char *strdup(const char *s)
{
	int size;
	char *copy;
	size = strlen(s) + 1;
	if ((copy = malloc(size)) == NULL)
		return NULL;
	memcpy(copy, s, size);
	return copy;
}

#endif /* strdup */
