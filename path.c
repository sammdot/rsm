#include <cairo.h>
#include <math.h>
#include "path.h"
#include "vector.h"

static const double EPSILON = 1e-16;
static const double PI = 3.1415926535897932384626433832795;

typedef struct path_builder_
{
	cairo_t *cr;
	int num;
	int last_moveto;
} path_builder;

typedef struct path_parser_
{
	path_builder pb;
	cairo_path_data_t cp;
	cairo_path_data_t rp;
	char cmd;
	int param;
	int rel;
	double params[7];
} path_parser;

static void path_moveto(path_builder *pb, double x, double y)
{
	cairo_move_to(pb->cr, x, y);
}

static void path_lineto(path_builder *pb, double x, double y)
{
	cairo_line_to(pb->cr, x, y);
}

static void path_curveto(path_builder *pb, double x1, double y1, double x2, double y2, double x3, double y3)
{
	cairo_curve_to(pb->cr, x1, y1, x2, y2, x3, y3);
}

static void path_close(path_builder *pb, double *x, double *y)
{
	cairo_get_current_point(pb->cr, x, y);
	cairo_close_path(pb->cr);
}

static void path_arc_segment(path_parser *pp, double xc, double yc, double t0, double t1, double rx, double ry, double xr)
{
	double x1, y1, x2, y2, x3, y3;
	double t;
	double t2;
	double f, sinf, cosf;

	f = xr * PI / 180.0;
	sinf = sin(f);
	cosf = cos(f);

	t2 = 0.5 * (t1 - t0);
	t = (8.0 / 3.0) * sin(t2 * 0.5) * sin(t2 * 0.5);
	x1 = rx * (cos(t0) - t * sin(t0));
	y1 = ry * (sin(t0) - t * cos(t0));
	x3 = rx * cos(t1);
	y3 = ry * sin(t1);
	x2 = x3 + rx * t * sin(t1);
	y2 = y3 + ry * -t * cos(t1);

	path_curveto(&pp->pb, xc + cosf * x1 - sinf * y1, yc + sinf * x1 + cosf * y1,
	            xc + cosf * x2 - sinf * y2, yc + sinf * x2 + cosf * y2,
	            xc + cosf * x3 - sinf * y3, yc + sinf * x3 + cosf * y3);
}

static void path_arc(path_parser *pp, double rx, double ry, double xr, int laf, int sf, double x, double y)
{
	double f, sinf, cosf;
	double x1, y1, x2, y2;
	double x1_, y1_;
	double cx_, cy_, cx, cy;
	double g, t1, dt;
	double k1, k2, k3, k4, k5;

	int i, n;

	x1 = pp->cp.point.x;
	y1 = pp->cp.point.y;
	x2 = x;
	y2 = y;

	if (x1 == x2 && y1 == y2)
		return;

	f = xr * PI / 180.0;
	sinf = sin(f);
	cosf = cos(f);

	if ((fabs(rx) < EPSILON) || (fabs(ry) < EPSILON))
	{
		path_lineto(&pp->pb, x, y);
		return;
	}

	if (rx < 0) rx = -rx;
	if (ry < 0) ry = -ry;

	k1 = (x1 - x2) / 2;
	k2 = (y1 - y2) / 2;
	x1_ = cosf * k1 + sinf * k2;
	y1_ = cosf * k2 - sinf * k1;

	g = (x1_ * x1_) / (rx * rx) + (y1_ * y1_) / (ry * ry);
	if (g > 1)
	{
		rx *= sqrt(g);
		ry *= sqrt(g);
	}

	k1 = rx * rx * y1_ * y1_ + ry * ry * x1_ * x1_;
	if (k1 == 0) return;

	k1 = sqrt(fabs((rx * rx * ry * ry) / k1 - 1));
	if (sf == laf) k1 = -k1;

	cx_ = k1 * rx * y1_ / ry;
	cy_ = -k1 * ry * x1_ / rx;

	cx = cosf * cx_ - sinf * cy_ + (x1 + x2) / 2;
	cy = sinf * cx_ + cosf * cy_ + (y1 + y2) / 2;

	k1 = (x1_ - cx_) / rx;
	k2 = (y1_ - cy_) / ry;
	k3 = (-x1_ - cx_) / rx;
	k4 = (-y1_ - cy_) / ry;

	k5 = sqrt(fabs(k1 * k1 + k2 * k2));
	if (k5 == 0) return;

	k5 = k1 / k5;
	if (k5 < -1) k5 = -1;
	else if (k5 > 1) k5 = 1;
	t1 = acos(k5);
	if (k2 < 0) t1 = -t1;

	k5 = sqrt(fabs((k1 * k1 + k2 * k2) * (k3 * k3 + k4 * k4)));
	if (k5 == 0) return;

	k5 = (k1 * k3 + k2 * k4) / k5;
	if (k5 < -1) k5 = -1;
	else if (k5 > 1) k5 = 1;
	dt = acos(k5);
	if (k1 * k4 - k3 * k2 < 0) dt = -dt;

	if (sf && dt < 0)
		dt += PI * 2;
	else if (!sf && dt > 0)
		dt -= PI * 2;

	n = ceil(fabs(dt / (PI * 0.5 + 0.001)));

	for (i = 0; i < n; ++i)
		path_arc_segment(pp, cx, cy, t1 + i * dt / n,
		               t1 + (i + 1) * dt / n, rx, ry, xr);
	pp->cp.point.x = x;
	pp->cp.point.y = y;
}

static void path_parse_default_xy(path_parser *pp, int n)
{
	int i;

	if (pp->rel)
		for (i = pp->param; i < n; ++i)
		{
			if (i > 2)
				pp->params[i] = pp->params[i - 2];
			else if (i == 1)
				pp->params[i] = pp->cp.point.y;
			else if (i == 0)
				pp->params[i] = pp->cp.point.x;
		}
	else
		for (i = pp->param; i < n; ++i)
			pp->params[i] = 0.0;
}

static void path_parse_do_cmd(path_parser *pp, int final)
{
	double x1, y1, x2, y2, x3, y3;

	switch (pp->cmd)
	{
		case 'm':
			if (pp->param == 2 || final)
			{
				path_parse_default_xy(pp, 2);
				path_moveto(&pp->pb, pp->params[0], pp->params[1]);
				pp->cp.point.x = pp->rp.point.x = pp->params[0];
				pp->cp.point.y = pp->rp.point.y = pp->params[1];
				pp->param = 0;
				pp->cmd = 'l';
			}
			break;
		case 'l':
			if (pp->param == 2 || final)
			{
				path_parse_default_xy(pp, 2);
				path_lineto(&pp->pb, pp->params[0], pp->params[1]);
				pp->cp.point.x = pp->rp.point.x = pp->params[0];
				pp->cp.point.y = pp->rp.point.y = pp->params[1];
				pp->param = 0;
			}
			break;
		case 'c':
			if (pp->param == 6 || final) {
				path_parse_default_xy(pp, 6);
				x1 = pp->params[0];
				y1 = pp->params[1];
				x2 = pp->params[2];
				y2 = pp->params[3];
				x3 = pp->params[4];
				y3 = pp->params[5];
				path_curveto(&pp->pb, x1, y1, x2, y2, x3, y3);
				pp->rp.point.x = x2;
				pp->rp.point.y = y2;
				pp->cp.point.x = x3;
				pp->cp.point.y = y3;
				pp->param = 0;
			}
			break;
		case 's':
			if (pp->param == 4 || final) {
				path_parse_default_xy(pp, 4);
				x1 = 2 * pp->cp.point.x - pp->rp.point.x;
				y1 = 2 * pp->cp.point.y - pp->rp.point.y;
				x2 = pp->params[0];
				y2 = pp->params[1];
				x3 = pp->params[2];
				y3 = pp->params[3];
				path_curveto(&pp->pb, x1, y1, x2, y2, x3, y3);
				pp->rp.point.x = x2;
				pp->rp.point.y = y2;
				pp->cp.point.x = x3;
				pp->cp.point.y = y3;
				pp->param = 0;
			}
			break;
		case 'h':
			if (pp->param == 1) {
				path_lineto(&pp->pb, pp->params[0], pp->cp.point.y);
				pp->cp.point.x = pp->rp.point.x = pp->params[0];
				pp->param = 0;
			}
			break;
		case 'v':
			if (pp->param == 1) {
				path_lineto(&pp->pb, pp->cp.point.x, pp->params[0]);
				pp->cp.point.y = pp->rp.point.y = pp->params[0];
				pp->param = 0;
			}
			break;
		case 'q':
			if (pp->param == 4 || final) {
				path_parse_default_xy(pp, 4);
				x1 = (pp->cp.point.x + 2 * pp->params[0]) * (1.0 / 3.0);
				y1 = (pp->cp.point.y + 2 * pp->params[1]) * (1.0 / 3.0);
				x3 = pp->params[2];
				y3 = pp->params[3];
				x2 = (x3 + 2 * pp->params[0]) * (1.0 / 3.0);
				y2 = (y3 + 2 * pp->params[1]) * (1.0 / 3.0);
				path_curveto(&pp->pb, x1, y1, x2, y2, x3, y3);
				pp->rp.point.x = pp->params[0];
				pp->rp.point.y = pp->params[1];
				pp->cp.point.x = x3;
				pp->cp.point.y = y3;
				pp->param = 0;
			}
			break;
		case 't':
			if (pp->param == 2 || final) {
				double xc, yc;

				xc = 2 * pp->cp.point.x - pp->rp.point.x;
				yc = 2 * pp->cp.point.y - pp->rp.point.y;

				x1 = (pp->cp.point.x + 2 * xc) * (1.0 / 3.0);
				y1 = (pp->cp.point.y + 2 * yc) * (1.0 / 3.0);
				x3 = pp->params[0];
				y3 = pp->params[1];
				x2 = (x3 + 2 * xc) * (1.0 / 3.0);
				y2 = (y3 + 2 * yc) * (1.0 / 3.0);
				path_curveto(&pp->pb, x1, y1, x2, y2, x3, y3);
				pp->rp.point.x = xc;
				pp->rp.point.y = yc;
				pp->cp.point.x = x3;
				pp->cp.point.y = y3;
				pp->param = 0;
			} else if (final) {
				if (pp->param > 2) {
					path_parse_default_xy(pp, 4);
					x1 = (pp->cp.point.x + 2 * pp->params[0]) * (1.0 / 3.0);
					y1 = (pp->cp.point.y + 2 * pp->params[1]) * (1.0 / 3.0);
					x3 = pp->params[2];
					y3 = pp->params[3];
					x2 = (x3 + 2 * pp->params[0]) * (1.0 / 3.0);
					y2 = (y3 + 2 * pp->params[1]) * (1.0 / 3.0);
					path_curveto(&pp->pb, x1, y1, x2, y2, x3, y3);
					pp->rp.point.x = pp->params[0];
					pp->rp.point.y = pp->params[1];
					pp->cp.point.x = x3;
					pp->cp.point.y = y3;
				} else {
					path_parse_default_xy(pp, 2);
					path_lineto(&pp->pb, pp->params[0], pp->params[1]);
					pp->cp.point.x = pp->rp.point.x = pp->params[0];
					pp->cp.point.y = pp->rp.point.y = pp->params[1];
				}
				pp->param = 0;
			}
			break;
		case 'a':
			if (pp->param == 7 || final) {
				path_arc(pp, pp->params[0], pp->params[1],
				        pp->params[2], pp->params[3],
				        pp->params[4], pp->params[5],
				        pp->params[6]);
				pp->param = 0;
			}
			break;
		default:
			pp->param = 0;
	}
}

static void path_end_of_num(path_parser *pp, double val, int sign, int exp_sign, int exp)
{
	val *= sign * pow (10, exp_sign * exp);
	if (pp->rel) {
		switch (pp->cmd) {
			case 'l':
			case 'm':
			case 'c':
			case 's':
			case 'q':
			case 't':
				if ((pp->param & 1) == 0)
					val += pp->cp.point.x;
				else if ((pp->param & 1) == 1)
					val += pp->cp.point.y;
				break;
			case 'a':
				if (pp->param == 5)
					val += pp->cp.point.x;
				else if (pp->param == 6)
					val += pp->cp.point.y;
				break;
			case 'h':
				val += pp->cp.point.x;
				break;
			case 'v':
				val += pp->cp.point.y;
				break;
		}
	}
	pp->params[pp->param++] = val;
	path_parse_do_cmd(pp, 0);
}

static void path_data_parse(path_parser *pp, char *d)
{
	int i = 0;
	double val = 0;
	char c = 0;
	int in_num = 0;
	int in_frac = 0;
	int in_exp = 0;
	int exp_wait_sign = 0;
	int sign = 0;
	int exp = 0;
	int exp_sign = 0;
	double frac = 0.0;

	in_num = 0;
	for (i = 0;; i++) {
		c = d[i];
		if (c >= '0' && c <= '9') {
			if (in_num) {
				if (in_exp) {
					exp = (exp * 10) + c - '0';
					exp_wait_sign = 0;
				} else if (in_frac)
					val += (frac *= 0.1) * (c - '0');
				else
					val = (val * 10) + c - '0';
			} else {
				in_num = 1;
				in_frac = 0;
				in_exp = 0;
				exp = 0;
				exp_sign = 1;
				exp_wait_sign = 0;
				val = c - '0';
				sign = 1;
			}
		} else if (c == '.') {
			if (!in_num) {
				in_frac = 1;
				val = 0;
			}
			else if (in_frac) {
				path_end_of_num(pp, val, sign, exp_sign, exp);
				in_frac = 0;
				in_exp = 0;
				exp = 0;
				exp_sign = 1;
				exp_wait_sign = 0;
				val = 0;
				sign = 1;
			}
			else {
				in_frac = 1;
			}
			in_num = 1;
			frac = 1;
		} else if ((c == 'E' || c == 'e') && in_num) {
			in_exp = 1;
			exp_wait_sign = 1;
			exp = 0;
			exp_sign = 1;
		} else if ((c == '+' || c == '-') && in_exp) {
			exp_sign = c == '+' ? 1 : -1;
		} else if (in_num) {
			path_end_of_num(pp, val, sign, exp_sign, exp);
			in_num = 0;
		}

		if (c == '\0')
			break;
		else if ((c == '+' || c == '-') && !exp_wait_sign) {
			sign = c == '+' ? 1 : -1;
			val = 0;
			in_num = 1;
			in_frac = 0;
			in_exp = 0;
			exp = 0;
			exp_sign = 1;
			exp_wait_sign = 0;
		} else if (c == 'z' || c == 'Z') {
			if (pp->param)
				path_parse_do_cmd(pp, 1);
			double x, y;
			cairo_path_data_t t;
			path_close(&pp->pb, &x, &y);

			t.point.x = x;
			t.point.y = y;
			pp->cp = t;
			pp->rp = t;
		} else if (c >= 'A' && c <= 'Z' && c != 'E') {
			if (pp->param)
				path_parse_do_cmd(pp, 1);
			pp->cmd = c + 'a' - 'A';
			pp->rel = 0;
		} else if (c >= 'a' && c <= 'z' && c != 'e') {
			if (pp->param)
				path_parse_do_cmd(pp, 1);
			pp->cmd = c;
			pp->rel = 1;
		}
	}
}

void path_draw(cairo_t *cr, char *d)
{
	path_parser pp;
	pp.pb.cr = cr;
	pp.cp.point.x = 0.0;
	pp.cp.point.y = 0.0;
	pp.cmd = 0;
	pp.param = 0;

	path_data_parse(&pp, d);

	if (pp.param)
		path_parse_do_cmd(&pp, 1);
}
